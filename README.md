## TAKE-HOME-ASSIGNMENT-SCSAC-DEVELOPER

##### CREATE A **HELLOWORLD** WEB APPS IN REACTJS THAT, AT LEAST, HAS THE FOLLOWING:  
1. A menu that can navigate two pages, a todo list and a weather forecast page  **[done, pagination 100%]**
2. A todo list that can add, edit, remove and complete any items  **[done, 100]**
3. A page displaying real time weather forecast in coming 7 days, information can retrieve from any opensource weather api, e.g. OpenWeather. User can input a city/location and the weather forecast will fetch and update accordingly  **[done, 100%]**
4. Dockerize your deliverable into container for deployment **[done, 100%]**

# Run dev environment via docker-compose

```
otto-int> $ `cd helloworld`
otto-int/helloworld> $ `docker-compose up dev -d`
```