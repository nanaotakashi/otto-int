/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'

const Forecast = ({daily} : any) => (
  <li className="text-info row">
    <div className="col-sm-6 text-start">
      <span>Time</span>
    </div>
    <div className="col-sm-6 text-start">
      <span>{daily.dt_txt}</span>
    </div>
    <div className="col-sm-6 text-start">
      <span>Temp</span>
    </div>
    <div className="col-sm-6 text-start">
      <span>Min. {Math.floor(daily.main.temp_min - 273)}°C</span> <span>Max. {Math.floor(daily.main.temp_max - 273)}°C</span>
    </div>
    <div className="col-sm-6 text-start">
      <span>Weather Description</span>
    </div>
    <div className="col-sm-6 text-start">
      <span>{daily.weather ? JSON.stringify(daily.weather[0].description) : ''}</span>
    </div>
  </li>
)

const Forecasts = ({ forecasts }: { forecasts: Array<any> }) => (
  <ul>
    {forecasts.map( (forecast: any) =>
      <Forecast
        daily={forecast}
      />
    )}
  </ul>
)

export default Forecasts
