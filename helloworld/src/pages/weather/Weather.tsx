/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState, useEffect } from 'react';
import { useStateValue } from '../../store';
import {
  getWeatherInfo,
  updateWeatherInfo
} from '../../store/reducer';

import Forecasts from './Forecasts'
import './Weather.css'
import config from '../../config/local.json';

const openWeatherApi = config.apiSource.openWeather;

const Weather = () => {
  const [currentCity, setCurrentCity] = useState('');
  const [forecasts, setForecasts] = useState([]);
  const [{ city, city: { weatherInfo } }, dispatch] = useStateValue();

  useEffect(() => {
    setCurrentCity(city.name);
  }, [city.name]);

  /**
   * Listen to value change on input box
   *
   * @param event Value-change event from HTMLInputElement - text input box
   * @see api refer to https://openweathermap.org/current#cityid
   */
  const handleCityNameInputChange = (event: { target: HTMLInputElement; }) => {
    setCurrentCity(event.target.value)
  }

  /**
   * Listen to every char input / keyUp
   *
   * @param event Keyup event from HTMLInputElement - text input box
   * @see event[.key,.keyCode] refer https://w3c.github.io/uievents/tools/key-event-viewer.html
   * @see api refer to https://openweathermap.org/current#cityid
   */
   const handleCityNameInputKeyUp = (event:  React.KeyboardEvent) => {
    if (event.keyCode === 13 || event.key === 'Enter') {
        event.preventDefault();

        /**
         * Get Current Weather Info
         */
        let currentWeatherApi = `${openWeatherApi.origin}data/${openWeatherApi.version}/weather?q=${encodeURIComponent(currentCity)}&appid=${openWeatherApi.appId}`;    
        console.log(`currentWeatherApi =>`, currentWeatherApi);
        getWeatherInfo(currentWeatherApi)
          .then(data => {
            console.log(`data =>`, data);
            updateWeatherInfo(dispatch, data);
          })
          .catch(err => console.log(err))
        
        /**
         * Get 7-day Weather Forecast
         */
        let forecastApi = `${openWeatherApi.origin}data/${openWeatherApi.version}/forecast?q=${encodeURIComponent(currentCity)}&cnt=7&appid=${openWeatherApi.appId}`;    
        console.log(`forecastApi =>`, forecastApi);
        getWeatherInfo(forecastApi)
          .then(data => {
            console.log(`forecastApi data =>`, data);
            setForecasts(data.list)
          })
          .catch(err => console.log(err))

    }
  }

  return (
    <div className="sub-main-weather">
      <small>
        Oops, No luck.
        <p>The API for daily forecast is no longer valid <a href="https://api.openweathermap.org/data/2.5/forecast/daily?q=Sheung%20Wan&cnt=7&appid=c7b5980bebc9988e7fce64db904ba617">https://api.openweathermap/../forecast/daily?...</a>,</p>
        <p>But I still found this 3hr forecasting is still valid <a href="https://api.openweathermap.org/data/2.5/forecast?q=Sheung%20Wan&cnt=7&appid=c7b5980bebc9988e7fce64db904ba617">https://api.openweathermap.org/../forecast?...</a></p>
      </small>
      <hr />
      <h2 className="text-danger">Weather</h2>
      <div className="mt-4 row">
        <label htmlFor="cityName" className="col-sm-3 text-end form-label">City Name</label>
        <div className="col-sm-6">
          <input type="text" className="form-control" id="cityName" autoComplete="off"
            value={currentCity}
            onKeyUp={handleCityNameInputKeyUp}
            onChange={handleCityNameInputChange}
          />
        </div>
      </div>
      <div className="mt-4 row">
        <div className="col-sm-12">
          <p className="text-left text-info">Current Weather: { JSON.stringify(weatherInfo ? weatherInfo.description : '') }</p>
          <p className="text-left text-info">Current Temperature: { JSON.stringify(city ? [city.temp, '°C'].join('') : '') }</p>
          <Forecasts forecasts={forecasts}/>
        </div>
      </div>
    </div>
  );
}

export default Weather;