/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState } from 'react';
import {
  getGeoIpInfo,
  updateCityInfo,
  getWeatherInfo,
  updateWeatherInfo
} from '../../store/reducer';
import { useStateValue } from '../../store';
import config from '../../config/local.json';

const openWeatherApi = config.apiSource.openWeather;

const Home = () => {
  const [currentCity, setCurrentCity] = useState('');
  const [{ city, city: { weatherInfo } }, dispatch] = useStateValue();

  /**
   * Gather local geoIpInfo and get Local Weather Info
   */
  const getCurrentLocationWeather = () => {
    // 1st fire
    getGeoIpInfo()
      .then(data => {
        setCurrentCity(data.city);
        updateCityInfo(dispatch, data);

        // 2nd fire
        let api = `http://api.openweathermap.org/data/2.5/weather?lat=${data.latitude}&lon=${data.longitude}&appid=${openWeatherApi.appId}`;
        getWeatherInfo(api)
          .then(data => {
            updateWeatherInfo(dispatch, data);
          })
          .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
  };

  useState(() => {
    getCurrentLocationWeather();
  });

  return (
    <div>
      <h2 className="text-danger">Hello World - Home</h2>
      <div className="mt-4 row">
        <div className="col-sm-12">
          <p className="text-center text-primary">Your current Location: </p>
          <p className="text-center text-success">{currentCity}, {city.name}</p>
        </div>
      </div>
      <div className="mt-4 row">
        <div className="col-sm-12">
          <p className="text-center text-info">Current Weather: { JSON.stringify(weatherInfo ? weatherInfo.description : '') }</p>
          <p className="text-center text-info">Current Temperature: { JSON.stringify(city ? [city.temp, '°C'].join('') : '') }</p>
        </div>
      </div>
    </div>
  );
}

export default Home;