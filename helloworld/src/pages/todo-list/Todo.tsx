/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { uuid } from 'uuidv4';
import {
  removeItem,
  editItem,
  addItem
} from '../../store/reducer';
import { useStateValue } from '../../store';

/**
 * For Adding item use
 * @returns JSX template
 */
export const ModifiedTodo = ({
  mode = 'add',
  item = { uuid: '', heading: '', content: '' },
  cancelEvent = () => {},
  okEvent = (value: any) => {}
}) => {
  const [isAddBtnToggle, setIsAddBtnToggle] = useState(mode === 'add' ? false : true);
  const [heading, setHeading] = useState(mode === 'add' ? '' : item.heading);
  const [content, setContent] = useState(mode === 'add' ? '' : item.content);
  const [{ todos }, dispatch] = useStateValue();

  /**
   * Listen to value change on input box
   *
   * @param event Value-change event
   */
  const handleHeadingKeyUp = (event: { target: any; }) => {
    setHeading(event.target.value)
  }

  /**
   * Listen to value change on textarea box
   *
   * @param event Value-change event
   */
   const handleContentKeyUp = (event: { target: any; }) => {
    setContent(event.target.value)
  }

  /**
   * For Cancel button
   */
  const handleCancelButton = () => {
    if (mode === 'add') {
      setIsAddBtnToggle(!isAddBtnToggle);
      setHeading('');
      setContent('');
    } else {
      cancelEvent();
    }
  }

  /**
   * For OK button
   */
  const handleOkButton = () => {
    if (mode === 'add') {
      setIsAddBtnToggle(!isAddBtnToggle);
      addItem(dispatch, {
        uuid: uuid(),
        timestamp: Date.now(),
        heading: heading,
        content: content
      })
      setHeading('');
      setContent('');
    } else {
      okEvent({
        uuid: item.uuid,
        timestamp: Date.now(),
        heading: heading,
        content: content
      })
    }
  }

  return (
    <div className="list-group-item list-group-item-action">
      <div
        className={`text-center ${isAddBtnToggle ? 'd-none' : ''}`}
      >
        <button type="button" className="btn btn-primary"
          onClick={() => { setIsAddBtnToggle(!isAddBtnToggle) }}
        >
          Add Todo here
        </button>
      </div>
      <div className={`row ${isAddBtnToggle ? '' : 'd-none'}`}>
        <label htmlFor="heading" className="col-sm-3 text-end form-label">New Heading</label>
        <div className="col-sm-6">
          <input type="text" className="form-control" id="heading" autoComplete="off"
            value={heading}
            onKeyUp={handleHeadingKeyUp}
            onChange={handleHeadingKeyUp}
          />
        </div>
      </div>
      <div className={`row ${isAddBtnToggle ? '' : 'd-none'}`}>
        <label htmlFor="content" className="col-sm-3 text-end form-label">New Content</label>
        <div className="col-sm-6">
          <textarea className="form-control" id="content" autoComplete="off"
            value={content}
            onKeyUp={handleContentKeyUp}
            onChange={handleContentKeyUp}
          />
        </div>
      </div>
      <div
        className={`text-center ${isAddBtnToggle ? '' : 'd-none'}`}
      >
        <button type="button" className="btn btn-default"
          onClick={handleCancelButton}
        >
          Cancel
        </button>
        <button type="button" className="btn btn-success"
          onClick={handleOkButton}
        >
          Confirm
        </button>
      </div>
    </div>
  )
};

/**
 * For listing item use
 * @param Object any
 * @returns JSX template
 */
export const Todo = ({ item }: any) => {
  const [isEdit, setIsEdit] = useState(false);
  const [{ todos }, dispatch] = useStateValue();

  const handleEditButton = () => {
    setIsEdit(!isEdit)
  }

  const handleOkButton = (item: any) => {
    editItem(dispatch, item)
    setIsEdit(!isEdit)
    // setTimeout(() => {
    //   isTodoListUpdated()
    // }, 500)
  }

  return (
    <div className="row">
      <div className="list-group-item list-group-item-action">
        <div className="d-flex w-100 justify-content-between">
          <h5 className="mb-1">{item.heading} - {new Date(item.timestamp).toLocaleString().split(',')[0]}</h5>
          <div className="text-muted btn-group" role="group" aria-label="Basic example">
            <button type="button" className="btn btn-secondary" onClick={handleEditButton}>Edit</button>
            <button type="button" className="btn btn-danger" onClick={() => {removeItem(dispatch, item)}}>Remove</button>
          </div>
        </div>
        <p className="mb-1">{item.content}</p>
      </div>
      <div className={`${isEdit ? '' : 'd-none'}`}>
        <ModifiedTodo
          mode={'edit'}
          item={item}
          cancelEvent={() => { setIsEdit(!isEdit) }}
          okEvent={handleOkButton}
        />
      </div>
    </div>
  )
};
