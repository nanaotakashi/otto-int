/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState, useEffect } from 'react';
// import {
// } from '../../store/reducer';
import { useStateValue } from '../../store';
import './TodoList.css';

import { Todo, ModifiedTodo } from './Todo';

const TodoList = () => {
  const [TodoList, setTodoList] = useState([]);
  const [{ todos }, dispatch] = useStateValue();

  useEffect(() => {
    setTodoList(todos)
  }, [ todos ])

  return (
    <div className="sub-main-todo-list">
      <h2>Todo List</h2>
      <hr />

      <div className="list-group">
        {TodoList.map( (todo: any) =>
          <Todo
            key={todo.uuid}
            item={todo}
          />
        )}
        <ModifiedTodo />
      </div>
    </div>
  );
};
export default TodoList;