import Home from '../pages/home/Home'
import TodoList from '../pages/todo-list/TodoList'
import Weather from '../pages/weather/Weather'

const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div></div>,
    main: () => <Home />
  },
  {
    path: "/todo-list",
    sidebar: () => <div></div>,
    main: () => <TodoList />
  },
  {
    path: "/weather",
    sidebar: () => <div></div>,
    main: () => <Weather />
  }
];

export default routes;
