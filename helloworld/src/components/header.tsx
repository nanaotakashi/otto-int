/* eslint-disable jsx-a11y/anchor-is-valid */
import { Component } from 'react';
import {
  NavLink
} from 'react-router-dom';

type Props = {};
type State = { isNavbarCollapsed: boolean };
class Header extends Component<Props, State> {
  
  constructor(props: any) {
    super(props);
    this.state = { 
      isNavbarCollapsed: true
    };
    this.toggleNavbarCollapse = this.toggleNavbarCollapse.bind(this);
  }

  toggleNavbarCollapse = () => {
    this.setState({ isNavbarCollapsed: !this.state.isNavbarCollapsed });
  };

  render () {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">Helloworld</NavLink>
          <button onClick={this.toggleNavbarCollapse} className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className={`navbar-collapse ${this.state.isNavbarCollapsed ? 'collapse' : ''}`}
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li onClick={this.toggleNavbarCollapse} className="nav-item">
                <NavLink exact={true} className="nav-link" activeClassName='active' to="/">Home</NavLink>
              </li>
              <li onClick={this.toggleNavbarCollapse} className="nav-item">
                <NavLink exact={true} className="nav-link" activeClassName='active' to="/todo-list">Todo List</NavLink>
              </li>
              <li onClick={this.toggleNavbarCollapse} className="nav-item">
                <NavLink exact={true} className="nav-link" activeClassName='active' to="/weather">Weather</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;