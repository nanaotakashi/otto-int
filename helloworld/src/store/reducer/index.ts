import { AnyAction } from 'redux';

/**
 * For Todo.tsx usage
 * @param dispatch any
 * @param data any
 */
export const removeItem = (dispatch: any, data: any) => {
  dispatch({
    type: 'REMOVE_TODO_SINGLE_ITEM_BY_UUID',
    payload: data.uuid
  });
};

/**
 * For Todo.tsx usage
 * @param dispatch any
 * @param data any
 */
 export const editItem = (dispatch: any, data: any) => {
  console.log(data)
  dispatch({
    type: 'EDIT_TODO_SINGLE_ITEM_BY_UUID',
    payload: data
  });
};

/**
 * For Todo.tsx usage
 * @param dispatch any
 * @param data any
 */
export const addItem = (dispatch: any, data: any) => {
  let buffer: any = {};
  dispatch({
    type: 'ADD_TODO_SINGLE_ITEM',
    payload: data
  });
};

/**
 * For Weather.tsx usage
 * @returns promise
 */
export const getGeoIpInfo = async () => {
  const res = await fetch('https://ipapi.co/json', { method: 'GET' })
  if (!res.ok) {
    throw new Error(`${res.status}, ${res.statusText}`);
  } else {
      return await res.json();
  }
};

/**
 * For Weather.tsx usage
 * @returns promise
 */
export const getWeatherInfo = async (apiEndPoint: string) => {
  const res = await fetch(apiEndPoint)
  if (!res.ok) {
    throw new Error(`${res.status}, ${res.statusText}`);
  } else {
      return await res.json();
  }
};

/**
 * For Weather.tsx usage
 * @param dispatch any
 * @param data any
 */
export const updateWeatherInfo = (dispatch: any, data: any) => {
  let buffer: any = data;
  dispatch({
    type: 'UPDATE_CITY_WEATHER',
    payload: buffer
  });
};

/**
 * For Weather.tsx usage
 * @param dispatch any
 * @param data any
 */
export const updateCityInfo = (dispatch: any, data: any) => {
  let buffer: any = data;
  buffer.name = data.city;
  buffer.long = data.longitude;
  buffer.lat = data.latitude;
  dispatch({
    type: 'UPDATE_CITY',
    payload: buffer
  });
};


/**
 * For all PAGEs usage
 * @param state any
 * @param action AnyAction
 * @returns state
 */
const reducer = (state: any, action: AnyAction) => {
  const { type, payload } = action;
  let oldTodos: Array<any> = state.todos;
  let modifiedTodos: Array<any> = [];
  switch (type) {
    case 'UPDATE_CITY':
      return {
        ...state,
        city: payload
      };
    case 'UPDATE_CITY_WEATHER':
      return {
        ...state,
        city: {
          weatherInfo: payload.weather[0],
          temp: Math.floor(payload.main.temp - 273),
          long: payload.coord.lon,
          lat: payload.coord.lat,
          name: payload.name
        }
      };
    case 'REMOVE_TODO_SINGLE_ITEM_BY_UUID':
      oldTodos = state.todos;
      modifiedTodos = [];
      modifiedTodos = oldTodos.filter(( obj ) => {
        return obj.uuid !== payload;
      });
      return {
        ...state,
        todos: modifiedTodos
      }
    case 'ADD_TODO_SINGLE_ITEM':
      oldTodos = state.todos;
      modifiedTodos = [];
      modifiedTodos = oldTodos.concat(payload);
      return {
        ...state,
        todos: modifiedTodos
      }
    case 'EDIT_TODO_SINGLE_ITEM_BY_UUID':
      oldTodos = state.todos;
      modifiedTodos = [];
      modifiedTodos = oldTodos.map(( obj ) => {
        console.log(obj.uuid === payload.uuid)
        return obj.uuid === payload.uuid
          ? payload
          : obj
      });
      return {
        ...state,
        todos: modifiedTodos
      }
    default:
      return state;
  }
}

export default reducer;