import { createContext, useContext } from 'react';

export const initialState: any = {
  city: {
    name: '',
    long: 0,
    lat: 0,
    temp: 0,
    weatherInfo: null
  },
  todos: [
    {
      uuid: 'cdba4c7a-d81c-4539-b9f4-d5fb9e04498c',
      heading: 'Sample #1',
      timestamp: 1627844037028,
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
    {
      uuid: 'b2f64a39-0901-4bf5-bb18-072b5aaff74c',
      heading: 'Sample #2',
      timestamp: 1627844037028,
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    },
    {
      uuid: '64e39931-9fa8-423f-9844-c6e921444773',
      heading: 'Sample #3',
      timestamp: 1627844037028,
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    }
  ]
}

export const StateContext = createContext(initialState);

export const useStateValue = () => useContext(StateContext); // For other page use
